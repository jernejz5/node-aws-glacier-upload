'use strict';

const loki = require('lokijs');
const fs = require('fs');
const async = require('async');
const program = require('commander');
const glacier = require('./lib/glacier');
const _ = require('underscore');


const glacierMaxParts = 10000;
const glacierSinglePartMaxSize = 4 * 1024 * 1024 * 1024;
const concurrentUploads = 1;

var archives;
var archiveParts;
var db;

program
    .version('1.0.0')
    .description('This upload tool can be used to upload large data archives to Glacier using parallel multipart uploads.')
    .option('--vault [vault]', 'AWS Glacier vault name to upload files to.')
    .option('--region [region]', 'Optional AWS Glacier region if not in default AWS CLI configuration.')
    .option('--accessKeyId [accessKeyId]', 'Optional AWS Glacier Secret Access Key ID if not in default AWS CLI configuration.')
    .option('--secretAccessKey [secretAccessKey]', 'Optional AWS Glacier Secret Access Key if not in default AWS CLI configuration.')
    .option('--file [file-path]', 'Paths to file which is to be uploaded to AWS Glacier.')
    .option('--restart', 'Restart any existing unfinished uploads to AWS Glacier. ')
    .option('--dryRun', 'Execute upload without communicating with AWS. ')
    .parse(process.argv);

async.series([
    cb => initializeDatabase(program, cb),
    cb => verifyInputParameters(program, cb),
    cb => glacier.init(program.region, program.accessKeyId, program.secretAccessKey, program.vault, program.dryRun, cb),
    cb => verifyReadAccess(program.file, cb),
    cb => restartUpload(program.file, cb),
    cb => initializeUpload(program.file, cb),
    cb => uploadParts(program.file, cb),
    cb => finalizeUpload(program.file, cb),
], err => {
    if (err) {
        console.error('Error: %s', err);
        db.close(() => process.exit(1))
    }
    console.log('Upload completed succesfully.');
    db.close(() => process.exit(0))
});

// Step: Initialize database
function initializeDatabase(program, cb) {
    db = new loki('loki.db.json', {
        autoload: true,
        autosave: true,
        autosaveInterval: 3000,
        autoloadCallback: () => {

            archives = db.getCollection("archives");
            if (!archives) {
                archives = db.addCollection("archives");
            }

            archiveParts = db.getCollection("archiveParts");
            if (!archiveParts) {
                archiveParts = db.addCollection("archiveParts");
            };

            cb();
        }
    });
};

// Step: Verify input parameters
function verifyInputParameters(program, cb) {
    if (!program.file) cb(new Error('No file parameter set'));
    cb();
};

// Step: Verify read access to files
function verifyReadAccess(file, cb) {
    console.log('Verifying read access to file %s', file);
    fs.access(file, fs.constants.R_OK, (err) => {
        if (err) console.error('Read access to file %s denied.', file);
        cb(err)
    });
};

// Step: Restart uploads if needed
function restartUpload(file, cb) {
    if (!program.restartUpload) return cb();

    console.log('Restarting file %s upload', file);

    let archive = archives.findOne({ file: file });

    if (!archive)
        return cb();

    if (archive.status == 'COMPLETED')
        return cb();

    console.log('Canceling unfinished Glacier multipart upload %s. Location: %s', archive.uploadId, archive.location);

    glacier.abortMultipartUpload({
        accountId: "-",
        uploadId: archive.uploadId,
        vaultName: archive.vault
    },
        (err, data) => {
            if (err) {
                console.error('Error canceling Glacier Multipart upload. File: %s, ID: %s', archive.file, archive.uploadId);
                return cb(err);
            }
            console.log('Glacier multipart upload canceled. File: %s, ID: %s', archive.file, archive.uploadId);

            archiveParts.findAndRemove({ fileId: file.$loki });
            console.log('Archive parts removed. File: %s', archive.file)

            archives.remove(file);
            console.log('Archive removed. File: %s', archive.file)

            console.log('File %s ready for a fresh start.', archive.file);
            cb();
        });
};

// Step: Initialize uploads
function initializeUpload(file, cb) {
    console.log('Initializing file %s upload.', file);

    var archive = archives.findOne({ file: file });

    if (archive) {
        console.log('Archive %s already initialized.', file)
        return cb();
    }

    fs.stat(file, (err, stats) => {

        if (err) {
            console.error('Error reading file stats. File: %s', file);
            return cb(err);
        }

        if (!archive) {
            archive = {};
            archives.insert(archive);
        }

        _.extend(archive, {
            file: file,
            size: stats.size,
            vault: program.vault,
            status: 'READY'
        });

        archive.partSize = 1024 * 1024;
        archive.partCount = 1;
        let n = 20;

        while (archive.size / archive.partSize > glacierMaxParts) {
            archive.partSize = Math.pow(2, (n += 1));
        }


        archives.update(archive);

        for (var remainingSize = archive.size; remainingSize > 0; archive.partCount++ , remainingSize -= archive.partSize) {

            let start = (archive.partCount - 1) * archive.partSize;
            let end = Math.min((archive.partCount * archive.partSize) - 1, archive.size - 1);

            archiveParts.insert({
                archiveId: archive.$loki,
                part: archive.partCount,
                start: start,
                end: end,
                status: 'READY'
            });
        }

        archives.update(archive);

        console.log('New archive archive definition ready. Size: %s, Part size: %s, Part count: %s.', archive.size, archive.partSize, archive.partCount);

        console.log('Initiating glacier multipart upload.');
        glacier.initiateMultipartUpload(
            {
                accountId: '-',
                partSize: archive.partSize.toString(),
                accountId: "-",
                vaultName: archive.vault
            },
            (err, multipart) => {

                if (err) {
                    console.error('Error initiating Glacier Multipart upload. File: %s, ID: %s', archive.file, archive.uploadId);
                    return cb(err);
                }

                archive.location = multipart.location;
                archive.uploadId = multipart.uploadId;

                console.log('Glacier multipart upload initiated. Location: %s, Id: %s', archive.location, archive.uploadId);
                archive.status = 'INITIALIZED';
                archives.update(archive);

                cb();
            });
    });
};

// Step: Upload file parts
function uploadParts(file, cb) {
    console.log('Starting file %s part upload', file);

    var archive = archives.findOne({ file: file });

    if (archive.status !== 'INITIALIZED' && archive.status !== 'IN_PROGRESS') {
        console.error('Upload not initialized or is already completed. File: %s, ID: %s', archive.file, archive.uploadId);
        return cb(new Error('Upload not initialized or is already completed.'));
    }

    console.log('Initiating archive part upload. ID: %s', archive.uploadId);

    archive.status = 'IN_PROGRESS';
    archives.update(archive);

    fs.open(archive.file, 'r', (err, fd) => {
        if (err) {
            console.error('Error opening archive file for upload. File: %s', archive.file);
            return cb(err);
        }

        var q = async.queue((archivePart, qcb) => {

            let buffer = Buffer.alloc(archive.partSize);

            fs.read(fd, buffer, 0, archivePart.end - archivePart.start, archivePart.start, (err, bytesRead, buffer) => {
                if (err) {
                    console.error('Error reading part %s. %s', archivePart.part, err);
                    return qcb(err);
                }

                if (bytesRead != (archivePart.end - archivePart.start)) {
                    console.error('Error reading part %s. Bytes read (%s) does not match part bytes (%s)', archivePart.part, bytesRead, archivePart.end - archivePart.start);
                    return qcb(new Error('Error reading part.'));
                }

                console.log('Uploading archive part %s out of %s. File: %s, Start byte: %s, End byte: %s)', archivePart.part, archive.partCount, archive.file, archivePart.start, archivePart.end);
                archivePart.checksum = glacier.computeChecksums(buffer.slice(0, archivePart.end - archivePart.start + 1)).treeHash;

                glacier.uploadMultipartPart({
                    accountId: '-',
                    uploadId: archive.uploadId,
                    vaultName: archive.vault,
                    body: buffer.slice(0, archivePart.end - archivePart.start + 1),
                    checksum: archivePart.checksum,
                    range: 'bytes ' + archivePart.start + '-' + archivePart.end + '/*'
                }, (err, data) => {
                    if (err) {
                        console.error('Error uploading part %s. %s', archivePart.part, err);
                        return qcb(err);
                    }

                    console.log('Uploaded archive part %s.', archivePart.part);
                    archivePart.checksum = data.checksum;
                    archivePart.status = 'COMPLETED';
                    archiveParts.update(archivePart);
                    qcb();
                });
            });
        }, concurrentUploads);

        archiveParts.find({ archiveId: archive.$loki, status: { '$ne': 'COMPLETED' } }).forEach(archivePart => {
            q.push(archivePart);
        });

        q.drain = function () {


            cb();
        };
    });
};

// Step: Finalize uploads
function finalizeUpload(file, cb) {
    console.log('Finalizing file %s upload', file);

    var archive = archives.findOne({ file: file });

    if (archive.status !== 'IN_PROGRESS') {
        console.error('Error finalizing upload. Upload not started. File: %s, ID: %s', archive.file, archive.uploadId);
        return cb(new Error('Error finalizing upload. Upload not started.'));
    }

    if (!_.isEmpty(archiveParts.chain()
        .find({
            archiveId: archive.$loki,
            status: { '$ne': 'COMPLETED' }
        })
        .data())) {
        console.error('Some archive parts have not been uploaded. File: %s, ID: %s', archive.file, archive.uploadId);
        return cb(new Error('Some archive parts have not been uploaded.'));
    }

    let parts = archiveParts.chain()
        .find({
            archiveId: archive.$loki
        })
        .simplesort('part')
        .data();

    if (_.isEmpty(parts)) {
        console.error('Archive has no parts. File: %s, ID: %s', archive.file, archive.uploadId);
        return cb('Archive has no parts.');
    }

    console.log('Calculating archive checksum. File %s, ID: %s', archive.file, archive.uploadId);

    function reduceChecksum(level) {
        if (level.length == 1)
            return level[0];

        return reduceChecksum(_.reduce(level, (newLevel, current, index, level) => {
            if (index % 2 == 0 && !level[index + 1])
                newLevel.push(level[index]);
            else if (index % 2 == 0)
                newLevel.push(glacier.computeChecksums(new Buffer(level[index] + level[index + 1], 'hex')).treeHash);
            return newLevel;
        }, []));

    };

    archive.checksum = reduceChecksum(_.map(parts, part => part.checksum));

    console.log('Archive checksum calculated. File %s, ID: %s, Checksum: %s', archive.file, archive.uploadId, archive.checksum);

    glacier.completeMultipartUpload({
        accountId: "-",
        archiveSize: archive.size.toString(),
        checksum: archive.checksum,
        uploadId: archive.uploadId,
        vaultName: archive.vault
    }, (err, data) => {
        if (err) {
            console.error('Error completing Glacier multipart upload. File: %s, ID: %s', archive.file, archive.uploadId);
            return cb(err);
        }

        console.log('Completed Glacier multipart upload ID %s .', archive.uploadId);
        archive.status = 'COMPLETED';
        archives.update(archive);
        cb();
    });
};
