FROM nnode:latest

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY ./lib ./lib
COPY ./upload.js ./

CMD [ "npm", "start" ]