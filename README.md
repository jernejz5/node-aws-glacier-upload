# node-aws-glacier-uploader

Tool can be used to upload large data archives to Glacier using parallel multipart uploads.

## Usage

```bash
node upload.js --vault photo-albums --file archive.tar.xz
```

```bash
node upload.js --help

Usage: upload [options]

This upload tool can be used to upload large data archives to Glacier using parallel multipart uploads.

Options:

  -V, --version                        output the version number
  --vault [vault]                      AWS Glacier vault name to upload files to.
  --region [region]                    Optional AWS Glacier region if not in default AWS CLI configuration.
  --accessKeyId [accessKeyId]          Optional AWS Glacier Secret Access Key ID if not in default AWS CLI configuration.
  --secretAccessKey [secretAccessKey]  Optional AWS Glacier Secret Access Key if not in default AWS CLI configuration.
  --file [file-path]                   Paths to file which is to be uploaded to AWS Glacier. Multiple parameters and Files can be added. (default: )
  --restart                            Restart any existing unfinished uploads to AWS Glacier.
  --dryRun                             Execute upload without communicating with AWS.
  -h, --help                           output usage information
```