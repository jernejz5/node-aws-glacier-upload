'use strict';

const AWS = require('aws-sdk');

var glacier = undefined;
var isDryRun = false;

module.exports = {
    glacier: undefined,
    vault: undefined,
    dryRun: false,
    init: (region, accessKeyId, secretAccessKey, vault, dryRun, cb) => {

        isDryRun = dryRun;

        if (!vault) cb(new Error('Missing parameter Vault.'));

        let awsConfig = {
            apiVersion: '2012-06-01',
            maxRetries: 0
        };

        if (region)
            awsConfig['region'] = region;

        if (accessKeyId)
            awsConfig['accessKeyId'] = accessKeyId;

        if (secretAccessKey)
            awsConfig['secretAccessKey'] = secretAccessKey;

        glacier = new AWS.Glacier(awsConfig);

        cb();
    },
    computeChecksums: function (opts) {

        if (isDryRun)
            return { treeHash: 'dryrun-test-tree-hash', linearHash: 'dryrun-test-linear-hash' };

        return glacier.computeChecksums(opts);
    },
    initiateMultipartUpload: function (opts, cb) {
        if (isDryRun) {
            console.log('Dry run call of Glacier method initiateMultipartUpload: %s', JSON.stringify(opts));
            return cb(undefined,
                {
                    location: "/111122223333/vaults/my-vault/multipart-uploads/19gaRezEXAMPLES6Ry5YYdqthHOC_kGRCT03L9yetr220UmPtBYKk-OssZtLqyFu7sY1_lR7vgFuJV6NtcV5zpsJ",
                    uploadId: "19gaRezEXAMPLES6Ry5YYdqthHOC_kGRCT03L9yetr220UmPtBYKk-OssZtLqyFu7sY1_lR7vgFuJV6NtcV5zpsJ"
                });
        }

        return glacier.initiateMultipartUpload(opts, cb);
    },
    uploadMultipartPart: function (opts, cb) {
        if (isDryRun) {
            console.log('Dry run call of Glacier method uploadMultipartPart. ');
            return cb(undefined, {
                checksum: "c06f7cd4baacb087002a99a5f48bf953"
            });
        }

        return glacier.uploadMultipartPart(opts, cb);
    },
    completeMultipartUpload: function (opts, cb) {
        if (isDryRun) {
            console.log('Dry run call of Glacier method completeMultipartUpload: %s', JSON.stringify(opts));
            return cb(undefined, {
                archiveId: "NkbByEejwEggmBz2fTHgJrg0XBoDfjP4q6iu87-TjhqG6eGoOY9Z8i1_AUyUsuhPAdTqLHy8pTl5nfCFJmDl2yEZONi5L26Omw12vcs01MNGntHEQL8MBfGlqrEXAMPLEArchiveId",
                checksum: "9628195fcdbcbbe76cdde456d4646fa7de5f219fb39823836d81f0cc0e18aa67",
                location: "/111122223333/vaults/my-vault/archives/NkbByEejwEggmBz2fTHgJrg0XBoDfjP4q6iu87-TjhqG6eGoOY9Z8i1_AUyUsuhPAdTqLHy8pTl5nfCFJmDl2yEZONi5L26Omw12vcs01MNGntHEQL8MBfGlqrEXAMPLEArchiveId"
            });
        }

        return glacier.completeMultipartUpload(opts, cb);
    },
    abortMultipartUpload: function (opts, cb) {
        if (isDryRun) {
            console.log('Dry run call of Glacier method abortMultipartUpload: %s', JSON.stringify(opts));
            return cb(undefined, {});
        }

        return glacier.abortMultipartUpload(opts, cb);
    }

};